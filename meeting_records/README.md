# Project Community Meeting

**NOTE**
1. This meeting introduction is for the project [community](/), not for the whole openEuler Community. 
2. Chinese is not supposed to provide for this README, as the contributors are from all of the world speaking many different languages. If you would like to provide a Chinese version, feel free to create a *README_cn.md* here.

### 1. Weekly meeting
- Time
    10:00-11:00 am, Tuesday, Beijing Time; 2:00-3:00 am, Tuesday, UTC.

- Channels

    Welink (<welink-meeting.zoom.us>) is used now. Every Monday the meeting ID will be sent out to <community@openeuler.org> and please check your calendar before the meeting. 

    If you have any issues, you can still ask help via IRC (<https://webchat.freenode.net/#openeuler-meeting>).     

    Updated on Mar 9, 2020, as the attendees prefer to use Welink, not IRC.
    ~~IRC will be used to have the weekly meeting. If you consider other meetings like zoom, please propose to [mail list](community@openeuler.org) in advance.~~

    ~~The channel https://webchat.freenode.net/#openeuler-meeting is used.~~

- Document for meetings

    If there are some documents for the meeting, please `cp ./meeting_records/template_meetig ./meeting_records/yyyy-mm-dd-meeting` and put the documents there so that attendees can find them. 

- Minutes of meeting

You can find [actions from the meeting](https://gitee.com/openeuler/community/wikis/Actions%20from%20Community%20governance%20weekly%20meeting?sort_id=1957419) here.

And you can find all the open tasks from [the wiki](https://gitee.com/openeuler/community/wikis/All%20the%20open%20tasks%20about%20openEuler%20community%20governance?sort_id=1929738).

### 2. Agenda for each meeting

* #topic Actions from last meeting

* #topic Issues reviewing

    Review the issues and talk on any issues we would to higlight.

* #topic Pull Request reviewing

    Review the PRs we consider important.

* #topic Topics raised by attendees

* #topic open discussion

### 3. Useful information
#### Frequently used meeting commands
THe common used commands are below. To find more, please visit [https://kiwiirc.com/docs/client/commands](https://kiwiirc.com/docs/client/commands) to find more commands. 

### Meeting Archives

Meeting minutes on Mar 10, 2020: https://mailweb.openeuler.org/hyperkitty/list/community@openeuler.org/thread/MI4276YKT5DZ4FJ2W6N4W5RT4VW5GK6X/

Meeting minutes on Mar 3, 2020: https://mailweb.openeuler.org/hyperkitty/list/community@openeuler.org/thread/TUMDUAX5UELXTAET6ZXIYXUYQQCL4B3Q/

Meeting minutes on Feb 25, 2020: https://mailweb.openeuler.org/hyperkitty/list/community@openeuler.org/thread/4LOWQMRP3CYUFIF2E2PT526S3VLA2XX5/

Meeting minutes on Feb 18, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-02-18-02.00.html

Meeting minutes on Feb 11, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-02-11-02.00.html

Meeting minutes on Fed 4, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-02-04-02.00.html

Meeting minutes on Jan 21, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-01-21-01.01.html

Meeting minutes on Jan 14, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-01-14-02.00.html

Meeting minutes on Jan 7, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-01-07-01.59.html

Meeting minutes on Jan. 5, 2020: http://meetings.openeuler.org/openeuler-meeting/2020/community/2020-01-05-13.13.html
